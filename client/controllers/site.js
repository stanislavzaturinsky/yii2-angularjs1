'use strict';
yii2AngApp_site.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/site/index', {
            templateUrl: 'views/site/index.html',
            controller : 'index'
        })
        .when('/site/about', {
            templateUrl: 'views/site/about.html',
            controller : 'about'
        })
        .otherwise({
            redirectTo: '/site/index'
        })
}])
.controller('index', ['$scope', '$http', function($scope, $http) {
    //Сообщение для отображения представлением
    $scope.message = 'Вы читаете главную страницу приложения.';
}])
.controller('about', ['$scope', '$http', function($scope, $http) {
    $scope.message = 'Данное приложение представляет простой пример по работе Yii2 в связке с AngularJS 1';
}]);
